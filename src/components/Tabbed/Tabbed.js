import { useState } from "react";
import styles from "./Tabbed.module.css";
function Tabbed() {
  const [tabs, setTabs] = useState(["Editor", "Content"]);
  const [selectedTab, setSelectedTab] = useState(0);
  return (
    <div className={styles.Tabbed}>
      <div className={styles.Tabs}>
        {tabs.map((t, index) => {
          return (
            <div
              key={index}
              onClick={() => {
                setSelectedTab(index);
              }}
              className={
                index === selectedTab
                  ? styles.Tab + " " + styles.TabActive
                  : styles.Tab
              }
            >
              {t}
            </div>
          );
        })}
      </div>
      <div className={styles.TabbedContent}></div>
    </div>
  );
}

export default Tabbed;
