import { useState } from "react";
import styles from "./AddBlockOptions.module.css";
function AddBlockOptions(props) {
  const [showAddBlockOptions, setShowAddBlockOptions] = useState(false);
  return (
    <div className={styles.addBlockOptions}>
      <button
        className={styles.addBlockOptionsButton}
        onClick={(e) => {
          e.stopPropagation();
          setShowAddBlockOptions(!showAddBlockOptions);
        }}
      >
        <img className={styles.Icon} src={"/add.svg"} />
      </button>
      {showAddBlockOptions ? (
        <div className={styles.addBlockOptionsOptions}>
          <span
            className={styles.addBlockOptionsOption}
            onClick={() => {
              props.addBlock("h1");
            }}
          >
            h1
          </span>
          <span
            className={styles.addBlockOptionsOption}
            onClick={() => {
              props.addBlock("ul");
            }}
          >
            <img className={styles.Icon} src={"/list.svg"} />
          </span>
          <span
            className={styles.addBlockOptionsOption}
            onClick={() => {
              props.addBlock("image");
            }}
          >
            <img className={styles.Icon} src={"/photo.svg"} />
          </span>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}

export default AddBlockOptions;
