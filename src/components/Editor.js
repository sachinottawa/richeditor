import { useEffect, useState } from "react";
import { v4 as uuid } from "uuid";
import AddBlockOptions from "./AddBlockOptions/AddBlockOptions";
import styles from "./Editor.module.css";
function Editor() {
  const [blocks, setBlocks] = useState({});
  const [showAddBlock, setShowAddBlock] = useState(true);
  const [handleVisibility, setHandleVibility] = useState(-1);
  useEffect(() => {}, [blocks]);
  const addBlock = (type) => {
    let key = uuid();
    let defaultVal;

    let newBlocks = {
      ...blocks,
      [key]: {
        type: type,
        value: "",
        data: [],
      },
    };
    setBlocks(newBlocks);
  };

  const removeBlock = (key) => {
    console.log(key);
    console.log(blocks);
    let newBlocks = [];
    Object.keys(blocks).map((k) => {
      if (key !== k) {
        newBlocks.push(blocks[key]);
      }
    });
    setBlocks(newBlocks);
  };

  const setText = (e, key) => {
    const newValue = e.target.innerText;
    let newBlocks = {
      ...blocks,
      [key]: {
        ...blocks[key],
        value: newValue,
      },
    };

    setBlocks(newBlocks);
  };

  const setData = (e, key) => {
    const newValue = e.target.innerText;
    console.log(newValue);
    const splitted = newValue.split("\n");
    const splittedWithoutEmpty = splitted.filter((t) => {
      return t.trim() !== "";
    });
    console.log(splittedWithoutEmpty);
    let newBlocks = {
      ...blocks,
      [key]: {
        ...blocks[key],
        data: [splittedWithoutEmpty],
      },
    };
    setBlocks(newBlocks);
    console.log(newBlocks);
  };

  const processKeyPress = (e, key) => {
    const type = blocks[key].type;
    switch (type) {
      case "h1":
        processH1(key, e);
        break;
      case "ul":
        processUl(key, e);
        break;
    }
  };
  const processH1 = (key, e) => {
    setText(e, key);
  };
  const processUl = (key, e) => {
    const keyPressed = e.which || e.keyCode;
    switch (keyPressed) {
      case 13 || 8:
        const currentText = blocks[key].value;
        setData(e, key);
        break;
      default:
        setText(e, key);
    }
  };
  return (
    <div
      className={styles.editor}
      onClick={() => {
        setShowAddBlock(!showAddBlock);
      }}
    >
      <h1>Editor</h1>
      {Object.keys(blocks).map((k) => {
        return (
          <div className={styles.blockEditor} key={k}>
            {console.log(handleVisibility)}
            <div
              contentEditable
              className={styles.block}
              onClick={(e) => {
                console.log(k);
                setHandleVibility(k);
              }}
              onBlur={(e) => {
                e.stopPropagation();
              }}
              onKeyUp={(e) => {
                processKeyPress(e, k);
              }}
            ></div>
            {handleVisibility === k ? (
              <div className={styles.blockLevelOptions}>
                <img
                  className={styles.blockLevelOption}
                  src={"/close.svg"}
                  onClick={() => {
                    removeBlock(k);
                  }}
                />
              </div>
            ) : (
              <></>
            )}
          </div>
        );
      })}
      {showAddBlock && (
        <AddBlockOptions
          addBlock={(type) => {
            addBlock(type);
          }}
        />
      )}

      <div className={styles.preview}>
        <h1>Preview</h1>

        {blocks &&
          Object.keys(blocks).map((k) => {
            //   return <h1 key={k}>{blocks[k].value}</h1>;
            switch (blocks[k].type) {
              case "h1":
                return <h1 key={k}>{blocks[k].value}</h1>;
              case "ul":
                return (
                  <ul key={k}>
                    {console.log(blocks[k].data[0])}
                    {/* {blocks[k].data.forEach((element) => {
                    console.log(`printing ${element}`);
                  })} */}
                    {blocks[k].data[0] &&
                      blocks[k].data[0].map((i, index) => {
                        // console.log(`printing ${index}`);
                        return <li key={index}>{i}</li>;
                      })}
                  </ul>
                );
            }
          })}
      </div>
    </div>
  );
}

export default Editor;
