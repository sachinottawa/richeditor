import Editor from "../components/Editor";
import Tabbed from "../components/Tabbed/Tabbed";
import styles from "./Layout.module.css";
function Layout() {
  return (
    <>
      <Tabbed />
      <Editor />
    </>
  );
}

export default Layout;
